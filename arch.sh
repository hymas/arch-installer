#!/bin/bash

ln -sf /usr/share/zoneinfo/Europe/Prague /etc/localtime
hwclock --systohc
sed -ri 's/#(en_US.UTF-8)/\1/' /etc/locale.gen
sed -ri 's/#(cs_CZ.UTF-8)/\1/' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "Pick a hostname for this machine:"
read varhost
echo $varhost >> /etc/hostname
echo "127.0.1.1      $varhost.localdomain      $varhost" >> /etc/hosts
curl http://sbc.io/hosts/alternates/fakenews-gambling-porn/hosts >> /etc/hosts

echo "Pick a root password:"
read varroot_password
echo root:$varroot_password | chpasswd

# uefi specific packages
# pacman -S efibootmgr

pacman -S grub networkmanager base-devel linux-zen-headers bluez bluez-utils pipewire gvfs alsa-utils pulseaudio zsh dash stow rustup

# packages for a graphical environment
pacman -S nm-connection-editor dialog wpa_supplicant ttf-roboto noto-fonts ttf-fira-code ttf-joypixels

grub-install --target=i386-pc /dev/sda
# grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable fstrim.timer

echo "Name your user:"
read varname
useradd -m $varname
echo "Pick a strong password for the user:"
read varpassword
echo $varname:$varpassword | chpasswd

usermod -aG wheel $varname
echo "$varname All=(All) ALL" >> /etc/sudoers.d/$varname

printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"
